package com.khojo.testxmpp;

import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    public static final String TAG="MainActivity";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
         super.onCreate(savedInstanceState);
        XMPPTCPConnectionConfiguration x =  XMPPTCPConnectionConfiguration.builder()
                .setHost("192.168.1.7")
                .setPort(5223)
                .setServiceName("spiderme.co")
                .setUsernameAndPassword("amandeep","bankam")
                .setDebuggerEnabled(true)
                .build();



        AbstractXMPPConnection y = new XMPPTCPConnection(x);
        try {
            y.connect();
            y.login();

            Chat chat = ChatManager.getInstanceFor(y).createChat("pooja@spiderme.co");
            chat.sendMessage("Hello");



           ChatManager.getInstanceFor(y).addChatListener(
                   new ChatManagerListener() {
                       @Override
                       public void chatCreated(Chat chat, boolean createdLocally) {
                           chat.addMessageListener(new ChatMessageListener() {
                               @Override
                               public void processMessage(Chat chat, Message message) {
                                   Log.e(TAG, message.toXML().toString());
                               }
                           });
                           Log.e("CHAT", "Message recieved");
                       }
                   });

        } catch (SmackException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
